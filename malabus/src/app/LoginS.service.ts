import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
const AUTH_API = 'http://localhost:8080/api/auth/signin';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})

export class ServiceLoginService {

    constructor(private http: HttpClient) { }

    login(username: string, password: string): Observable<any> {
      return this.http.post(AUTH_API + 'signin', {
        username,
        password
      }, httpOptions);
    }
}
